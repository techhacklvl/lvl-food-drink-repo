Aji de Gallina

Ingredients
4 yellow potatoes
4 slices white bread
3/4 cup evaporated milk
1 1/2 pounds chicken breast
4 cups chicken stock
3 to 4 yellow aji peppers
1/2 cup vegetable oil
2 cloves garlic (minced)
1 large onion (finely chopped)
3 tablespoons Parmesan cheese (grated)
2 hard-boiled eggs
10 black olives (halved)

Instructions
Gather the ingredients.
Cook the yellow potatoes in salted water until tender when pierced with a fork.
Let cool, peel, cut into quarters, and set aside.
Place the bread in a small bowl and pour the evaporated milk over it to soak. Set aside.
Place the chicken breasts in a pot with the chicken stock and bring to a simmer.
Cook for 10 to 15 minutes until chicken is just barely cooked through.
Set chicken aside to cool.
Strain broth and reserve 2 cups.
Remove stems and seeds from the peppers.
In a blender, process peppers with the vegetable oil until smooth.
Sauté the garlic and onions with the puréed peppers and oil until the onions are soft and golden.
Remove from heat and let cool.
Shred the cooled chicken into bite-size pieces.
In a blender or food processor, process the evaporated milk and bread mixture with Parmesan cheese until smooth.
Add the cooked onion mixture and process briefly.
Return onion mixture to pan, and add 1 1/2 cups of the reserved chicken stock.
Bring to a low simmer, and stir in the chicken. Heat until warmed through, adding more chicken stock if the sauce is too thick.
Serve over rice, garnished with the yellow potatoes, slices of hard-boiled egg and black olives.
